var mongoose = require('mongoose');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;
const mailer = require('../mailer/mailer');

const saltRounds = 10; //defino variable para darle aletoreidad al algoritmo de  encriptacion (hashSync)
const bcrypt = require ('bcrypt'); //Modulo que nos permite encryptar  debemos instalarlo porque NO ESTÁ en el Package.json (npm install bcrypt --save)
const uniqueValidator = require('mongoose-unique-validator'); // se instala con npm install mongoose-unique-validator --save

const crypto = require('crypto'); //para el TOKEN
const Token = require ('../models/token');

//Funcion con expresión regular para validar email:
const validateEmail = function(email){
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  //objeto regex, expresion regular
    return re.test(email);
};

//creamos el esquema de Modelo de usuario con los parámetros necesarios para la autenticacion Two Factor
var usuarioSchema = new Schema ({
    nombre: {
        type: String,
        trim: true, //borra los espacios del inicio de texto
        required: [true, 'El nombre es obligatorio']
    },
    email:{
        type: String,
        trim: true, //borra los espacios del inicio de texto
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        unique: true, 
        validate: [validateEmail, 'Por favor ingrese un email valido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/] //corre a nivel MONGODB
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio']
    },

    passwordResetToken: String, //el toquen se asociará a una cuenta de correo una vez se registre el usuario, deben estar encriptados
    passwordResetTokenExpires: Date, 
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: String, //para Oauth 2.0
    facebookId: String   // agregamos los Strings al modelo del usuario
  
});

//Agrego plugin Validator al Esquema:
usuarioSchema.plugin(uniqueValidator, {message: 'El {PATH} ya existe con otro usuario'});//hace referencia al PATH del email



//Funcion para que antes de guardar(save) encripte las claves 
usuarioSchema.pre('save',function(next){ //callback sobre el save
    if (this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds); //Salt le da una aleatoreidad a la encryptacion
    }
    next();

});
//agregamos comparacion del password a ver si el password es valido
//Para el login se pregunta si l password es valido y se compara con la encryptacion del mismo password, a ver si coincide 
usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.password);
}

//y le agregamos al esquema el metodo de la reserva
usuarioSchema.methods.reservar = function(biciId, desde,hasta, cb){
    var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta})
    console.log(reserva);
    reserva.save(cb);
}

//metodo para enviar email de bienvenida a usuarios nuevos : REVISAR PORQUE NO FUNCIONÓ
usuarioSchema.methods.enviar_email_bienvenida = function(cb) {
    //especificamos TOKEN usando la librería crypto requerida arriba
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    //creamos una constante con el email que le corresponde al usuario:
    const email_destination = this.email;
    token.save(function (err){//persistimos los datos
        if (err) { return console.log(err.message);}

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Verificacion de cuenta', //token.token es el token encryptado para que no muestre el original en la URL y no se pueda hackear
            text: 'Hola,\n\n' + 'Por favor, para verificar su cuenta haga click en este link: \n' + 'http://localhost:3000' + '\/token/confirmation\/'+ token.token + '.\n'
        };
        //una vez tengamos instalada la librería mailer y tengamos la carpeta con el codigo losto
        mailer.sendMail(mailOptions, function (err){
            if (err) { return console.log(err.message); } //esto es asincronico, si hay un error le logea por consola y el metodo se finaliza

            console.log('Se ha enviado un email de bienvenida a:' + email_destination + '.');
        });
    });


}
//Metodo para Strategy de Google  en Passport.js el cual sirve para OAuth 2.0

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrcreate(condition, callback) { //se pone condition porque a la funcion le entra una condicion de busqueda ó de creacion en caso de que no esté registrado
    const self = this;
    console.log(condition);
    self.findOne({
        $or:[
            {'googleId': condition.id}, {'email': condition.emails[0].value}
        ]}, (err, result)=> {
                if (result) { //Entra por el result ya que no va a ser nully saldrá por el callback. Esta parte es LOGIN
                    callback(err, result)
                }else { //esta parte es para un registro
                    console.log('=====CONDITION======');
                    console.log(condition);
                    let values = {};
                    values.googleId = condition.id;
                    values.email = condition.emails[0].value;
                    values.nombre = condition.displayName || 'SIN NOMBRE';
                    values.verificado = true;
                    values.password = crypto.randomBytes(16).toString('hex'); // Esta parte es importante porque json etag no siempre funciona, eso depende de los permisos que le pongamos. Igual la mejor practica es representar la estrategia de login en un objeto**
                    console.log('=====VALUES=======');
                    console.log(values);
                    self.create(values, (err, result)=>{
                        if (err) {console.log(err);}
                        return callback(err, result)
                    })
                }
        })
 };
 //Metodo Strategy para Oauth Facebook
 usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrcreate(condition, callback) { //se pone condition porque a la funcion le entra una condicion de busqueda ó de creacion en caso de que no esté registrado
    const self = this;
    console.log(condition);
    self.findOne({
        $or:[
            {'facebookId': condition.id}, {'email': condition.emails[0].value}
        ]}, (err, result)=> {
                if (result) { //Entra por el result ya que no va a ser nully saldrá por el callback. Esta parte es LOGIN
                    callback(err, result)
                }else { //esta parte es para un registro
                    console.log('=====CONDITION======');
                    console.log(condition);
                    let values = {};
                    values.facebookId = condition.id;
                    values.email = condition.emails[0].value;
                    values.nombre = condition.displayName || 'SIN NOMBRE';
                    values.verificado = true;
                    values.password = condition._json.etag;
                    console.log('=====VALUES=======');
                    console.log(values);
                    self.create(values, (err, result)=>{
                        if (err) {console.log(err);}
                        return callback(err, result)
                    })
                }
        })
 };


module.exports = mongoose.model('Usuario', usuarioSchema);