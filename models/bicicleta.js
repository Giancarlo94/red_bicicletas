//agregamos referencia mongoose para poder trabajar con ella:
var mongoose= require ('mongoose');
var Schema = mongoose.Schema;

//creo el esquema al cual se le pasará un JSON
var bicicletaSchema = new Schema({
    code: Number, //representa el id, no se pone "id" porque es una variable reservada en MongoDb
    color: String,
    modelo: String,
    ubicacion: { //ubicacion es de tipo lista de numeros
        type: [Number], index: { type: '2dsphere', sparse: true } //index/indice es una variable de tipo geografico
    }
});



//Metodos de instancia de mongoose:
//crear instancia del esquema MOngo DB(se hace para que el esquema pueda iteractuar con los atributos de la BD)
bicicletaSchema.statics.createInstance = function (code, color, modelo, ubicacion) {
    return new this({
        code : code,
        color : color,
        modelo : modelo,
        ubicacion : ubicacion

    });
   
};

bicicletaSchema.methods.toString = function(){
    return 'code: ' + this.code + ' | color: ' + this.color;
};

bicicletaSchema.statics.allBicis = function(cb){
    return this.find({}, cb);
};

bicicletaSchema.statics.add = function(aBici, cb){
    this.create(aBici, cb);
};

bicicletaSchema.statics.findByCode = function(cb){
    return this.findOne({code: aCode }, cb);
};
bicicletaSchema.statics.removeByCode = function(cb){
    return this.deleteOne({code: aCode }, cb);
};

//finalmente, exportamos el modelo anterior(bicicleta) el cual utilizara el anterior esquema
 
module.exports = mongoose.model('Bicicleta', bicicletaSchema);


