var mongoose = require ('mongoose');

//utilizamos librería moment ( se instala con npm install moment)
//nos permite trabajar con las fechas
var moment = require('moment');
var Schema = mongoose.Schema;

var reservaSchema = new Schema({
    desde: Date,
    hasta: Date,
    bicicleta: {type: mongoose.Schema.Types.ObjectId, ref: 'Bicicleta'},//lo guardamos como referencia para querer acceder más adelante al usuario que esta haciendo la reserva
    usuario: {type: mongoose.Schema.Types.ObjectId, ref: 'Usuario'},// igual
});

//metodo de instancia para saber cuantos dias estan reservando
reservaSchema.methods.diasDeReserva = function(){
    return moment(this.hasta).diff(moment(this.desde), 'days') + 1;//le sumo uno para considerar que son días, no es necesario

}

module.exports = mongoose.model('Reserva', reservaSchema);
