const nodemailer = require ('nodemailer'); //lo instalo con npm install nodemailer --save , esta librería me ayuda a simular un servidor de correos SMTP

const sgTransport = require('nodemailer-sendgrid-transport'); //Para envio de correos en ambiente de produccion

let mailConfig;

//Acá evalúo si estamos trabajando en ambiente de desarrollo o de producción
if (process.env.NODE_ENV === 'production'){
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_SECRET//Clave de la API, ya debe de estar en el servidor Heroku(Settings>COnfig vars) Y en el .env
        }
    }
    mailConfig = sgTransport(options);
}else {
    if (process.env.NODE_ENV === 'staging'){
        console.log('XXXXXXXXXXXX');
        const options = {
            auth: {
                api_key: process.env.SENDGRID_API_SECRET
            }
        }
        mailConfig = sgTransport(options);

    }else {
        mailConfig = {
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: 'alize.welch36@ethereal.email',
                pass: 'JwTP1RqHnf99ExbZeR'
            }

        };
    }
   
}

module.exports = nodemailer.createTransport(mailConfig);
