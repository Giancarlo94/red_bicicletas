var Bicicleta = require ('../../../models/bicicleta');

//nos traemos la librería request:
var request = require('request');
//traemos la variable que ejecuta el servidor para no tener que utilizarlo en dos consolas.
var server = require("../../../bin/www")


//Codigo para testeart el método GET
describe ('Bicicleta API', () => {
    describe ('GET BICICLETAS /', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta (1, 'negro', 'urbana', [-34.6012424, -58.3861497]);
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            });

        });
    });
    //Codigo para testeart el método POST
    describe ('POST BICICLETAS /create', () => {
        it('STATUS 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "id": 10, "color":"rojo", "modelo" : "urbana", "lat": -34, "lng": -54 }';
          
    
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
                
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("rojo");
                done();         
            });
    
        });
    });

});


// se puede testear también el DELTE y el UPDATE con una logica similar a la de arriba**



