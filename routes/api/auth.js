const express = require ('express');
const router = express.Router();
const passport = require ('../../config/passport');

const authController = require('../../controllers/api/authControllerAPI');

router.post('/authenticate', authController.authenticate);
router.post('/forgotPassword', authController.forgotPassword);
//Esta ruta tiene como middleware la autenticacion via token de facebook antes de redireccionar a /facebook_token
router.post('/facebook_token', passport.authenticate('facebook-token'), authController.authFacebookToken);// (?)


module.exports = router;
