var express = require('express');
var router = express.Router();

/* GET home page. Este codigo maneja las rutas*/
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' }); //Renderea el template de una vista (index=nombre del template, ubicado en la carpeta "views", seguido del objeto con los atributos)
});

module.exports = router;
