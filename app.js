//MODULO PRINCIPAL
require('newrelic');//necesaria para monitoreo
//Para indicarle a MOngo que utilizaremos las variables de ambiente del archivo .env:
require('dotenv').config();

//importamos todos los moludos que vamos a utilizar aquí
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');


//ya debe estar este archivo y esta carpeta creada:
const passport = require('./config/passport'); //para cookies y login se debe instalar : npm install passport --save

const session= require('express-session'); //para login $ npm install express-session --save

const jwt = require('jsonwebtoken') // Librería necesaria para poder hacer la funcion de validacion de User para las API

const MongoDBStore = require ('connect-mongodb-session')(session); // $ npm i connect-mongodb-session

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bicicletasRouter = require('./routes/bicicletas');
//defino ruta API
var bicicletasAPIRouter = require('./routes/api/bicicletas');

//agrego ruta Usuarios
var usuariosAPIRouter = require('./routes/api/usuarios');
var authAPIRouter = require ('./routes/api/auth');

const Usuario = require ('./models/usuario');
const Token = require ('./models/token');
//INICIO CONFIGURACION MONGO DB
// este objeto define cual es el MOTOR DE SESION que vamos a usar, DEBE IR ANTES DE VAR APP= EXPRESS()****
//const store = new session.MemoryStore;  se guardará la sesion en memoria del servidor, NO EN MONGO DB. la desventaja de este tipo es que: en caso de que el servidor se resetee, los usuarios deberan logearse nuevamente
let store;
if (process.env.NODE_ENV === 'development'){
  store = new session.MemoryStore;
}else{
  store =new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  });
  store.on('error', function(error){
    assert.ifError(error);
    assert.ok(false);
  });
}

let app = express();
app.set('secretKey','jwt_pwd_!!223344'); // Defino clave y valor para el token cifrado de la API, el codigo puede ser cualquiercosa.
//configuracion de la cookie y la sesion, debe ir acá, luego de var app
app.use(session({
  cookie: { maxAge: 240 * 60 * 60 * 1000}, //defino el tiempo de duracion de la cookie ( 240H*1H*1m*1ms)= 10 días
  store: store, //donde voy a guardar
  saveUninitialized: true,
  resave: 'true', 
  secret: 'red_bicis_!!!***!"-!"-!"-!"-!"-!"-123123' //se utiliza para generar la encryptacion del identificador de la cookie, es como una semilla que se manda al servidor
}));

//Traemos la referencia PARA USAR MONGO DB 
var mongoose = require('mongoose');

//Defino variables para TOKEN y USUARIOS:
var usuariosRouter = require('./routes/usuarios');
var tokenRouter = require ('./routes/token');


const { promises } = require('fs');
const usuario = require('./models/usuario');
const { token } = require('morgan');
const { assert } = require('console');

//Si estoy en ambiente/entorno de desarrollo, utilizo localhost, si estoy en produccion, utilizo ATLAS
//Cremos una variable para que tenga la conexión con la DB en la nube, Mongo atlas:
//mongodb+srv://admin:<password>@cluster-red-bicicletas.n4bya.mongodb.net/<dbname>?retryWrites=true&w=majority
//var mongoDB = 'mongodb+srv://admin:<eofCeE6VdgfVXMao>@cluster-red-bicicletas.n4bya.mongodb.net/<dbname>?retryWrites=true&w=majority';
var mongoDB = process.env.MONGO_URI; //Para que trabaje con el cluster en la nube, que está definido en el archivo .env

mongoose.connect(mongoDB,{ useNewUrlParser: true });
mongoose.Promise = global.Promise;
var db= mongoose.connection; //guardo la conexión en db
db.on('error', console.error.bind(console, 'MongoDB conection error: '));
//...FIN  CONFIGURACION MONGO DB


// view engine setup, definimos el motor y donde ir a buscar(pug) para el tema de las vistas
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// USAMOS TODOS LOS MODULOS
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
//inicializacion de passport, debe ir luego del cookie parser***
app.use(passport.initialize());
app.use(passport.session()); //para session


app.use(express.static(path.join(__dirname, 'public')));

//para que se visualicen las vistas de Login
app.get('/login', function(req, res){
  res.render('session/login');//nos redirije a session
});
//para posteo de login, validaciones
app.post('/login', function(req, res, next){
  //passport que recibe el callback de Local Strategy(passport.js) enviando los parametros res, req, next
  passport.authenticate('local',function(err, usuario, info){
    if (err) return next(err);
    if (!usuario) return res.render('session/login', {info});
    req.logIn(usuario, function(err){
      return res. redirect('/');
    });
    
  })(req, res, next);
});
//para el logout
app.get('/logout', function(req, res){
  req.logOut();
  res.redirect('/');
});

//para el forgotpassword
app.get('/forgotpassword', function(req,res){
  res.render('session/forgotPassword');
});
app.post('/forgotPassword', function(req,res){
  Usuario.findOne({ email: req.body.email }, function (err, usuario){
    if (!usuario) return res.render('session/forgotPassword',{info: {message: 'No existe usuario con este password'}});

    usuario.resetPassword(function(err){
      if (err) return next(err);
      console.log('session/forgotPasswordMessage');
    });
    res.render('session/forgotPasswordMessage');

  });

});
//para resetar el password
app.get('resetPassword/:token', function(req, res, next){
  Token.findOne({ token: req.params.token}, function(err, token){
    if (!token) return res.status(400).send({ type: 'not-verified', msg: 'No existe un usuario verificado con este dato'});

    Usuario.findById(token._userId, function (err, usuario){
      if (!usuario) return res.status(400).send({msg: 'No existe usuario asociado a ese correo'});
      res.render('session/resetPassword', {errors: {}, usuario: usuario});
    });
  });
});

app.post('/resetPassword',function(req, res){
  if (req.body.password != req.body.confirm_password) {
    res.render('session/resetPassword', {errors: {confirm_password: { message: ' No coincide el password'}}});
    return;
  }
  Usuario.findOne({ email: req.body.email }, function (err, usuario){
    usuario.password = req.body.password;
    usuario.save(function(err){
      if (err){
        res.render('session/resetPassword',{errors: err.errors, usuario: new Usuario()});
     }else{
       res.redirect('/login');
     }});
  });
});


//para usuarios y token 
app.use('/', indexRouter);
app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter);

app.use('/', indexRouter); //ubica el modulo IndexRouter en la ruta raiz '/'
app.use('/users', usersRouter); // La aplicacion indica al servidor que escuche en esta ruta lo que se le va a informar el modulo de rutas, redirecciona y acciona los métodos creados allá
app.use('/bicicletas', loggedIn, bicicletasRouter ); //esta ruta es especial para usuarios logeados, primero se ejecuta el loggedIn, y si cumple, pasa a la pagina bicicletas. Seguriza la ruta
//le decimos que use la ruta api
app.use('/api/bicicletas',validarUsuario, bicicletasAPIRouter);
//ruta API usuarios
app.use('/api/usuarios', usuariosAPIRouter); //funciona

app.use('/api/auth', authAPIRouter); //no funciona
app.use('/api/bicicletas', bicicletasAPIRouter); //funciona, porque me pide la autenticacion primero

//Verificacion de Google y ENLACE DE CREDENCIALES CON APIS

app.use('/google3209a1f00cfafcb3', function(req, res){
  res.sendFile('public/google3209a1f00cfafcb3.html');
});

//APIS 
app.get('/auth/google',
  passport.authenticate('google', { scope:
      [ 'https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/plus.profile.emails.read' ] }
));

app.get( '/auth/google/callback',
    passport.authenticate( 'google', {
        successRedirect: '/',
        failureRedirect: '/error'
}));


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');

});

//Funcion de seguridad(middleware para web)  para vincular en las rutas privadas para usuarios logeados, que si detecta un user NO logeado, lo redirecciona a /login
function loggedIn (req, res , next){
  if (req.user){
    next();
  }else{
    console.log('user sin loguearse');
    res.redirect('/login');
  }
};

//Validar User para la version de la web con API, que usa el SecretKey definido arriba luego de app.express, consta de una clave y un valor
function validarUsuario(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded){
    if(err) {
      res.json({status: "error", message: err.message, data: null});
    }else{
      req.body.userId = decoded.id;
      console.log('jwt verify:' + decoded);
      next();

    }
  });
}


module.exports = app;
