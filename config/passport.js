const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy; 
const Usuario = require('../models/usuario');
const GoogleStrategy = require('passport-google-oauth20'); // Para OAuth con Google y crear la estrategia
const FacebookTokenStrategy = require ('passport-facebook-token'); //para Oauth con Facebook


// COfiguramos la Strategy de Facebook

passport.use(new FacebookTokenStrategy({
    clientID:     process.env.FACEBOOK_ID,
    clientSecret: process.env.FACEBOOK_SECRET
     
  },
  function( accessToken, refreshToken, profile, done) {
      try{
        User.findOneOrCreateByFacebook(profile, function (err, user) {  //el metodo FindOneOrcreateByGoogle, lo creamos en models>usuario***
            if (err) console.log('err' + err );
            return done(err, user);                  
            });
         } catch(err2){
          console.log(err2);
          return done(err2, null);
      }

    }
  
));


 
//configuramos la estrategia local, este codigo se sacó de : http://www.passportjs.org/packages/passport-google-oauth2/
passport.use(new GoogleStrategy({
    clientID:     process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: process.env.HOST + "/auth/google/callback"
    
  },
  function( accessToken, refreshToken, profile, cb) {
      console.log(profile);

      Usuario.findOneOrCreateByGoogle(profile, function (err, user) {  //el metodo FindOneOrcreateByGoogle, lo creamos en models>usuario***
      return cb(err, user);
    });
  })
);

//configuramos la estrategia local para prueba en Localhost, no tiene que ver con APIS**
passport.use(new LocalStrategy(
       function(email, password, done) {
        Usuario.findOne({ email: email }, function (err, usuario) {
        if (err) { return done(err); }
       if (!usuario) {
       
       return done(null, false, { message: 'Email no existente o incorrecto.' });
        }
        if (!usuario.validPassword(password)) {
       
       return done(null, false, { message: 'Password incorrecto.' });
        }
        return done(null, usuario);
        });
        }
   ));

passport.serializeUser(function(user, cb){ //esta funcion toma el id del ususario una vez se inicie sesion
    cb(null, user.id);
});

passport.deserializeUser(function(id, cb){
    Usuario.findById(id, function(err, usuario){
        cb(err, usuario);

    });
});

module.exports = passport;